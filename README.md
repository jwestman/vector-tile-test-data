# Vector tile test data

These vector tiles are used to test the in-development vector renderer in
libshumate. I generated them using [martin](https://github.com/urbica/martin)
from [Natural Earth](https://www.naturalearthdata.com/) and
[osmdata.openstreetmap.de](https://osmdata.openstreetmap.de/data/water-polygons.html).

These tiles are intended as test data, not as a particularly useful map. Max
zoom is 5.
